
import socket
import random

PORT = 1239
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.bind(('localhost', PORT))

mySocket.listen(5)

random.seed()

try:
    while True:
        print("Waiting for connections...")
        (recvSocket, address) = mySocket.accept()
        print("HTTP request recieved")
        recieved = recvSocket.recv(2048)
        print(recieved)

        # Ahora creamos la siguiente URL para darle al navegador
        next_page = str(random.randint(0,10000000))
        next_url = ("http://localhost:" + str(PORT) + "/" + next_page)
        print("AQUI ESTÁ LA SIGUIENTE URL --------> ", next_url)
        respuesta = b"HTTP/1.1 301 Moved Permanently\r\n" \
        + b"Location: " + next_url.encode('utf-8')  + b"\r\n" \
        + b"\r\n"

        recvSocket.send(respuesta)
        recvSocket.close()

except KeyboardInterrupt:
    print("Closing server...")
    mySocket.close()
    
